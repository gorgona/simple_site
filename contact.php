<?php
$page_name = "Контакты";
include("./config.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>automafia.com.ua</title>
</head>
<body>
<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include(PATH_INFO."/block/left_block.php");
        ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include(PATH_INFO."/block/header_head.php");
        ?>
        <div  class = "content border">
            <div>
                <?php
                  include(PATH_INFO."/block/textContact.php");
                  include(PATH_INFO."/block/sheduler.php");
                  include(PATH_INFO."/block/managerTable.php");
                  include(PATH_INFO."/block/contactsProchie.php");
                ?>

            </div>
        </div>
        <div  class = "footer  border">

        </div>
    </div>


</div>

