<?php
$page_name = "Ответственность и правила";
include("./config.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>automafia.com.ua</title>

</head>
<body>
<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include(PATH_INFO."/block/left_block.php");
        ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include(PATH_INFO."/block/header_head.php");
        ?>
        <div  class = "content border">
             <p class="redText">Внимание!</p>
            <p class="violetText">Во избежание недоразумений делайте заказы только  по официальным номерам компании АВТОМАФИЯ.
            Все контакты и имена наших сотрудников указаны только на этом сайте в разделе <a class = "blueText hrefLine" href="contact.php">КОНТАКТЫ</a> Остерегайтесь мошенников представляющихся АВТОМАФИЕЙ.
            </p>
            <p class="redText">
            Внимание!
                </p>
            <p class="violetText">
            При оплате нам проверяйте платёжные реквизиты по нашему сайту. Актуальные и действительные платёжные реквизиты (карточка, счёт)
            указаны только на этом сайте на страничке <a class = "blueText hrefLine" href="oplata.php"> ОПЛАТА</a> При оплате заказов на иные реквизиты компания АВТОМАФИЯ снимает с себя всякую ответственность.
            </p>
            <p class="redText">
            Внимание!
                </p>
            <p class="violetText">
            Условия заказа запчастей из-за рубежа.
            </p>
            <p>
            <div  >
                <ul>
            <li class = "decimal">Возврат запчастей заказанных за рубежом невозможен.</li>
            <li class = "decimal">Предоплата 100%. При этом окончательная цена на данный заказ фиксируется.</li>
            <li class = "decimal">В связи со сложностью доставки запчастей из-за границы (несколько транзитных складов, граница, таможня,
                человеческий фактор, удалённость склада закупки и т.д.) срок исполнения заказа является ориентировочным и
                точного срока поставки мы гарантировать не можем.</li>
            <li class = "decimal">Отказ клиента от поставки товара после оформления заказа невозможен.</li>
            <li class = "decimal">При большой задержке заказа крайним сроком ожидания является срок 6недель с момента оформления заказа.
                Только по истечении данного срока возможен возврат предоплаты.</li>
            <li class = "decimal">После заказа товара возможен отказ поставщика от поставки по различным причинам (устаревший прайс, отсутствие товара на складе, утеря товара и т.д ).</li>
            <li class = "decimal">В случае отказа поставщика от поставки возможны варианты: заказ товара с другого склада (возможно изменение цены, согласовывается с клиентом) или возврат предоплаты.</li>
            <li class = "decimal">В случае невозможности поставки предоплата возвращается в исходном размере и в той же валюте что и была внесена.</li>
            </ul>
                </div>

        </div>
        <div  class = "footer  border">

        </div>
    </div>


</div>

