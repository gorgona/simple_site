<?php
$page_name = "Доставка в регионы";
include("./config.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>automafia.com.ua</title>
</head>
<body>

<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include(PATH_INFO."/block/left_block.php");
        ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include(PATH_INFO."/block/header_head.php");
        ?>
        <div  class = "content border">
            <p class = "redText">
            Рассылку товара осуществляем предпочтительно транспортными компаниями:
                </p>
            <ul class = "blueText">
            <li><a href="https://www.meest-express.com.ua">MEEST EXPRESS     https://www.meest-express.com.ua</a></li>
            <li><a href="http://www.gunsel.ua">ГЮНСЕЛ     http://www.gunsel.ua</a></li>
            <li><a href="http://novaposhta.ua">НОВА ПОШТА     http://novaposhta.ua</a></li>
            <li><a href="http://www.nexpress.com.ua">НОЧНОЙ ЭКСПРЕСС     http://www.nexpress.com.ua</a></li>
            <li><a href="http://www.sat-trans.com">САТ     http://www.sat-trans.com</a></li>
            </ul>

            <p class = "redText">   Осуществляем доставку по Харькову. </p>


        </div>
        <div  class = "footer  border">
            <?php
            include(PATH_INFO."/block/footer.php")
            ?>
        </div>
    </div>


</div>

