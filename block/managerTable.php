﻿    <table class="stripy classtable" id = "Manager">
        <tbody>
        <tr>
            <td><a href="tel:+380677915033">(067)791-50-33</a></td>
            <td><a href="tel:+380932903552">(093)290-35-52</a></td>
            <td><span class="violetText" ><a href="tel:+380503253538">(050)325-35-38 +Viber</a></span></td>
            <td>Сергей Желавский</td>
            <td>u0027@automafia.com.ua</td>
        </tr>
        <tr>
            <td><a href="tel:+380677913705">(067)791-37-05</a></td>
            <td><a href="tel:+380932903551">(093)290-35-51</a></td>
            <td><span class="violetText" ><a href="tel:+380503253537">(050)325-35-37 +Viber</a></span></td>
            <td>Менеджер 26</td>
            <td>u0026@automafia.com.ua</td>
        </tr>
        <tr>
            <td><a href="tel:+380677914086">(067)791-40-86</a></td>
            <td><a href="tel:+380930853036">(093)085-30-36</a></td>
            <td><span class="violetText" ><a href="tel:+380503253501">(050)325-35-01 +Viber</a></span></td>
            <td>Менеджер 25</td>
            <td>u0025@automafia.com.ua</td>
        </tr>
        <tr>
            <td><a href="tel:+380677395028">(067)739-50-28</a></td>
            <td><a href="tel:+380932881109">(093)288-11-09</a></td>
            <td><span class="violetText" ><a href="tel:+380503254959">(050)325-49-59 +Viber</a></span></td>
            <td>Менеджер 24</td>
            <td>u0024@automafia.com.ua</td>
        </tr>
        <tr>
            <td><a href="tel:+380675793124">(067)579-31-24</a></td>
            <td><a href="tel:+380637613912">(063)761-39-12</a></td>
            <td><span class="violetText" ><a href="tel:+380503252938">(050)325-29-38 +Viber</a></span></td>
            <td>Алла Булгакова</td>
            <td>u0018@automafia.com.ua</td>
        </tr>
        <tr>
            <td><a href="tel:+380675709211">(067)570-92-11</a></td>
            <td><a href="tel:+380937617525">(093)761-75-25</a></td>
            <td><span class="violetText" ><a href="tel:+380503257552">(050)325-75-52 +Viber</a></span></td>
            <td>Виктория Ройтих</td>
            <td>u0017@automafia.com.ua</td>
        </tr>
        <tr>
            <td><a href="tel:+380675793142">(067)579-31-42</a></td>
            <td><a href="tel:+380637613917">(063)761-39-17</a></td>
            <td><span class="violetText" ><a href="tel:+380503003922">(050)300-39-22 +Viber</a></span></td>
            <td>Юрий Маренич</td>
            <td>u0016@automafia.com.ua</td>
        </tr>

        <tr>
            <td><a href="tel:+380675793149">(067)579-31-49</a></td>
            <td><a href="tel:+380937617529">(093)761-75-29</a></td>
            <td><span class="violetText" ><a href="tel:+380503279086">(050)327-90-86 +Viber</a></span></td>
            <td>Алексей Шатный</td>
            <td>u0015@automafia.com.ua</td>
        </tr>
        <tr>
            <td><a href="tel:+380675462324">(067)546-23-24</a></td>
            <td><a href="tel:+380937617522">(093)761-75-22</a></td>
            <td><span class="violetText" ><a href="tel:+380503279034">(050)327-90-34 +Viber</a></span></td>
            <td>Олег Дудняк</td>
            <td>u0012@automafia.com.ua </td>
        </tr>

        <tr>
            <td><a href="tel:+380675602188">(067)560-21-88</a></td>
            <td><a href="tel:+380937617518">(093)761-75-18</a></td>
            <td><span class="violetText" ><a href="tel:+380503252923">(050)325-29-23 +Viber</a></span></td>
            <td>Александр Малышев</td>
            <td>u0011@automafia.com.ua</td>
        </tr>


        <tr>
            <td><a href="tel:+380675793160">(067)579-31-60</a></td>
            <td><a href="tel:+380937617523">(093)761-75-23</a></td>
            <td><span class="violetText" ><a href="tel:+380503278923">(050)327-89-23 +Viber</a></span></td>
            <td>Сергей Космаков</td>
            <td>u0010@automafia.com.ua</td>
        </tr>
        <tr>
            <td><a href="tel:+380675793138">(067)579-31-38</a></td>
            <td><a href="tel:+380937617521">(093)761-75-21</a></td>
            <td><span class="violetText" ><a href="tel:+380503275406">(050)327-54-06 +Viber</a></span></td>
            <td>Менеджер 9</td>
            <td>u0009@automafia.com.ua </td>
        </tr>

        <tr>
            <td><a href="tel:+380675793159">(067)579-31-59</a></td>
            <td><a href="tel:+380637613918">(063)761-39-18</a></td>
            <td><span class="violetText" ><a href="tel:+380503250066">(050)325-00-66 +Viber</a></span></td>
            <td>Евгений Маслий</td>
            <td>u0008@automafia.com.ua</td>
        </tr>
        <tr>
            <td><a href="tel:+38‎0675793136">(067)570-92-69</a></td>
            <td><a href="tel:+380637612473">(063)761-24-73</a></td>
            <td><span class="violetText" ><a href="tel:+380503257551">(050)325-75-51 +Viber</a></span></td>
            <td>Александр Дудык</td>
            <td>u0007@automafia.com.ua</td>
        </tr>


        <tr>
            <td><a href="tel:+380675793116">(067)579-31-16</a></td>
            <td><a href="tel:+380637613916">(063)761-39-16</a></td>
            <td><span class="violetText" ><a href="tel:+380503251325">(050)325-13-25 +Viber</a></span></td>
            <td>Андрей Сурков</td>
            <td>u0006@automafia.com.ua</td>
        </tr>

        <tr>
            <td><a href="tel:+380973287848">(097)328-78-48</a></td>
            <td><a href="tel:+380637613915">(063)761-39-15</a></td>
            <td><span class="violetText" ><a href="tel:+380503253519">(050)325-35-19 +Viber</a></span></td>
            <td>Менеджер 5</td>
            <td>u0005@automafia.com.ua</td>
        </tr>

        <tr>
            <td><a href="tel:+380675790829">(067)579-08-29</a></td>
            <td><a href="tel:+380637613914">(063)761-39-14</a></td>
            <td><span class="violetText" ><a href="tel:+380503252930">(050)325-29-30 +Viber</a></span></td>
            <td></td>
            <td>u0004@automafia.com.ua</td>
        </tr>


        <tr>
            <td><a href="tel:+380675793117">(067)579-31-17</a></td>
            <td><a href="tel:+380637613913">(063)761-39-13</a></td>
            <td><span class="violetText" ><a href="tel:+380503463936">(050)346-39-36 +Viber</a></span></td>
            <td>Константин Шетилов</td>
            <td>u0003@automafia.com.ua</td>
        </tr>

       <tr>
            <td><a href="tel:+380675793128">(067)579-31-28</a></td>
            <td><a href="tel:+380637613911">(063)761-39-11</a></td>
            <td><span class="violetText" ><a href="tel:+380503252937">(050)325-29-37 +Viber</a></span></td>
            <td>Вячеслав Пашкевич</td>
            <td>u0001@automafia.com.ua</td>
        </tr>
    </tbody>


    </table>




