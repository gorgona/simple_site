<?php
$page_name = "Главная";
include("./config.php");
?>

<?php

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>automafia.com.ua</title>
</head>

<body>

<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include("./block/left_block.php");
        ?>
    </div>

    <div id = "right" class = "right border">
        <div>
            <?php
            include("./block/header.php");
            include("./block/header_head.php");
            ?>
        </div>

        <div  class = "content border">
            <?php
              include("./block/textContact.php");
              include("./block/sheduler.php");
              include("./block/managerTable.php");
              include("./block/contactsProchie.php");

            ?>
        </div>
    </div>
</div>


