<?php
    $page_name = "ABESTA";
    include("./config.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>automafia.com.ua</title>
</head>
<body>
<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
       include(PATH_INFO."/block/left_block.php");
       ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include (PATH_INFO."/block/header_head.php");
        ?>

        <div  class = "content border">
        <p> Пропонуємо роздрібним і оптовим клієнтам амортизатори
           <span style = "color: red; font-style: italic ; font-weight: bolder; font-size: larger ">
            ABESTA
                 </span>
            прямо з нашого складу в Харкові.
            В наявності широкий асортимент амортизаторів на японські, корейські і європейські автомобілі.
            Висока якість амортизаторів
            <span style = "color: red; font-style: italic ; font-weight: bolder; font-size: larger ">
            ABESTA
                 </span>
            підтверджено багаторічною успішною експлуатацією виробів на дорогах України.
            Важливо, що дані амортизатори мають оптимальне співвідношення параметрів ціна / якість, що вигідно як кінцевого споживача,
            так і магазинам і СТО пропонує ці амортизатори в роздріб.
        </p>

        <p>
            На амортизатори
            <span style = "color: red; font-style: italic ; font-weight: bolder; font-size: larger ">
            ABESTA
                 </span>
            поширюється офіційна гарантія.
        </p>

        <p>
            <span style = "color: red;  font-weight: bolder; font-size: larger ">
            УВАГА!!!
            </span>
        </p>
        <p>
            Додатково, завжди в наявності комплекти пильовиків + відбійників на будь-який амортизатор.
            <ul>
                <li>SPO12770060 для діаметра штока 12-16мм (+SPO12)</li>
                <li>SPO18770045 для діаметра штока 18мм</li>
                <li>SPO20770042 для діаметра штока 20мм</li>
                <li>SPO22770046 для діаметра штока 22мм</li>
            </ul>
        </p>



        </div>
        <div  class = "footer  border">
            <?php
            include(PATH_INFO."/block/footer.php")
            ?>
        </div>
    </div>


</div>

