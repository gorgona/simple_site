<?php
$page_name = "Вакансії";
include("./config.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>automafia.com.ua</title>

</head>
<body>
<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include(PATH_INFO."/block/left_block.php");
        ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include (PATH_INFO."/block/header_head.php");
        ?>
        <div  class = "content border">
            <p>
            1. Менеджер з продажу автозапчастин.
                </p>
            <p>
                Вимоги: досвід роботи з TECDOC і оригінальними програмами по іномарках. </p>
            <ul>
                <li>097-301-99-22</li>
                <li>050-301-99-22</li>
                <li>063-301-99-22</li>
                <li> Олег Володимирович.</li>
            </ul>
        </div>
        <div  class = "footer  border">
            <?php
            include(PATH_INFO."/block/footer.php")
                ?>
        </div>
    </div>


</div>

