<?php
$page_name = "Вакансії";
include("./config.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>automafia.com.ua</title>

</head>
<body>
<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include(PATH_INFO."/block/left_block.php");
        ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include (PATH_INFO."/block/header_head.php");
        ?>
        <div  class = "content border">
            <p class="h5smart">
            1. Менеджер з продажу автозапчастин.
                </p>
            <p class="h6smart">
                Вимоги: досвід роботи з TECDOC і оригінальними програмами по іномарках. </p>
            <ul>
                <li> <p class="h6smart">097-301-99-22</p></li>
                <li><p class="h6smart">050-301-99-22</p></li>
                <li><p class="h6smart">063-301-99-22</p></li>
                <li><p class="h6smart"> Олег Володимирович.</p></li>
            </ul>
        </div>
        <div  class = "footer  border">
            <?php
            include(PATH_INFO."/block/footer.php")
                ?>
        </div>
    </div>


</div>

