<?php
$page_name = "Відповідальність і правила";
include("./config.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>automafia.com.ua</title>
    <link href="<?php echo(ADRES); ?>/css/official.css" rel="stylesheet">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

</head>
<body>
<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include(PATH_INFO."/block/left_block.php");
        ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include(PATH_INFO."/block/header_head.php");
        ?>
        <div  class = "content border">
             <p class="redText h4smart">Увага!</p>
            <p class="violetText h6smart">Щоб уникнути непорозумінь робіть замовлення тільки за офіційними номерами компанії АВТОМАФІЯ.
                Всі контакти і імена наших співробітників вказані тільки на цьому сайті в розділі <a class = "blueText hrefLine" href="contact.php">КОНТАКТЫ</a> Остерігайтеся шахраїв тих, що представляються АВТОМАФІЄЮ.
            </p>
            <p class="redText h4smart">
                УВАГА!
                </p>
            <p class="violetText h6smart">
                При оплаті нам перевіряйте платіжні реквізити по нашому сайту. Актуальні і дійсні платіжні реквізити (картка, рахунок)
                вказані тільки на цьому сайті на сторінкі <a class = "blueText hrefLine" href="oplata.php"> ОПЛАТА</a> При оплаті замовлень на інші реквізити компанія АВТОМАФІЯ знімає з себе всяку відповідальність.
            </p>
            <p class="redText h4smart">
                УВАГА!
                </p>
            <p class="violetText h5smart">
                Умови замовлення запчастин з-за кордону.
            </p>
            <p>
            <div  >
                <ul>
            <li class = "decimal h6smart">Повернення запчастин замовлених за кордоном неможливий.</li>
            <li class = "decimal h6smart">Передоплата 100%. При цьому остаточна ціна на це замовлення фіксується.</li>
            <li class = "decimal h6smart">У зв'язку із складністю доставки запчастин з-за кордону (декілька транзитних складів, межа, митниця,
                людський чинник, віддаленість складу закупівлі і так далі) термін виконання замовлення є орієнтовним і точного терміну постачання ми гарантувати не можемо</li>
            <li class = "decimal h6smart">Відмова клієнта від постачання товару після оформлення замовлення неможлива.</li>
            <li class = "decimal h6smart">При великій затримці замовлення крайнім терміном очікування є термін 6недель з моменту оформлення замовлення.
                Тільки після закінчення цього терміну можливе повернення передоплати.</li>
            <li class = "decimal h6smart">Після замовлення товару можлива відмова постачальника від постачання з різних причин (застарілий прайс, відсутність товару на складі, втрата товару і т.д ).</li>
            <li class = "decimal h6smart">У разі відмови постачальника від постачання можливі варіанти: замовлення товару з іншого складу (можлива зміна ціни, узгоджується з клієнтом) або повернення передоплати.</li>
            <li class = "decimal h6smart">У разі неможливості постачання передоплата повертається в початковому розмірі і в тій же валюті що і була внесена.</li>
            </ul>
                </div>

        </div>

    </div>
    <div  class = "footer  border">
        <?php
        include(PATH_INFO."/block/footer.php")
        ?>
    </div>

</div>

