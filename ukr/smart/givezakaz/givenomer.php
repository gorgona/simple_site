<?php
$page_name = "Заказ по номеру";
include("../config.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>automafia.com.ua</title>
    <link href="../css/official.css" rel="stylesheet">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
</head>
<body>
<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include(PATH_INFO."/block/left_block.php");
        ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include(PATH_INFO."/block/header_head.php");
        ?>
        <div  class = "content border">
            <div  class = "content border">
                <table>
                    <tr>
                        <td class="left-symbol"> <h1 class = "redText h1smart" >!</h1></td>
                        <td class="right-text">
                            <h3 class = "redText h6smart" >УВАЖАЕМЫЕ ПОКУПАТЕЛИ!</h3>
                            <h3 class = "redText h6smart" >ПРИ ПОЛУЧЕНИИ ТОВАРА ОБЯЗАТЕЛЬНО СВЕРЯЙТЕ НОМЕРА ЗАПЧАСТЕЙ И ИХ КОЛИЧЕСТВО С ВАШЕЙ РАСХОДНОЙ НАКЛАДНОЙ </h3>
                        </td>
                    </tr>
                </table>

                <div >
            <h1 class = "redText h5smart" > Продиктуйте номер вашего заказа по телефону ниже указанным сотрудникам:</h1>

            <?php
            include(PATH_INFO."/block/contactKassa.php");
            ?>
            <a href="<?php echo(ADRES); ?>give-zakaz.php"><button type="button" class="btn-primary lagbtn" >НАЗАД</button></a>

          </div>

</div>
</div>
</body>