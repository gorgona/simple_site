<?php
/**
 * Created by PhpStorm.
 * User: Elena
 * Date: 17.10.2019
 * Time: 15:08
 */


class mylib
{

    public function getPathInfo()
    {
        $_pathInfo = $this->resolvePathInfo();

        return $_pathInfo;
    }


    protected function resolvePathInfo()
    {
        $host = $_SERVER['HTTP_HOST'];
        $path = $_SERVER['REQUEST_URI'];
        $pathInfo = $host."".$path;


        if (($pos = strpos($pathInfo, '?')) !== false)
        {
            $pathInfo = substr($pathInfo, 0, $pos);
        }

        $pathInfo = urldecode($pathInfo);

        // try to encode in UTF8 if not so
        // http://w3.org/International/questions/qa-forms-utf-8.html
        if (!preg_match('%^(?:
            [\x09\x0A\x0D\x20-\x7E]              # ASCII
            | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
            | \xE0[\xA0-\xBF][\x80-\xBF]         # excluding overlongs
            | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
            | \xED[\x80-\x9F][\x80-\xBF]         # excluding surrogates
            | \xF0[\x90-\xBF][\x80-\xBF]{2}      # planes 1-3
            | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
            | \xF4[\x80-\x8F][\x80-\xBF]{2}      # plane 16
            )*$%xs', $pathInfo)
        )
        {
            $pathInfo = utf8_encode($pathInfo);
        }

        if (substr($pathInfo, 0, 1) === '/')
        {
            $pathInfo = substr($pathInfo, 1);
        }

        if( $str_index = strrchr($pathInfo,'/' )!= false)
        {
           $pathInfo = str_replace($str_index,"",$pathInfo);
        }
      return (string)$pathInfo;
    }
}
