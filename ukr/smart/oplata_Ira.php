<?php
$page_name = "Оплата";
include("./config.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <title>automafia.com.ua</title>
</head>
<body>
<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include(PATH_INFO."/block/left_block.php");
        ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include(PATH_INFO."/block/header_head.php");
        ?>
        <div  class = "content border">
            <h3 class="redText h3smart">Безготівковий розрахунок (UAH)</h3>

           <ul>
               <li class = "greenText"><p class="h5smart">ПАТ КБ ПРИВАТ БАНК</p></li>
               <li><p class="h5smart">р/с UA353515330000026001052205729</p></li>
               <li><p class="h5smart">МФО: 351533</p> </li>
               <li><p class="h5smart"> ЕДРПОУ: 2827021122 </p></li>
               <li style="font-weight: bolder"><p class="h5smart"> Власник рахунку: ФОП Лисенко Ірина Ігорівна </p></li>
           </ul>
           <a href="<?php echo(PATH_INFO); ?>/pravila.php"><h3 class = "violetText h3smart">ВІДПОВІДАЛЬНІСТЬ</h3></a>

        </div>
        <div  class = "footer  border">
            <?php
            include(PATH_INFO."/block/footer.php")
            ?>
        </div>
    </div>


</div>

