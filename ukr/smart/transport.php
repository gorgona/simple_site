<?php
$page_name = "Доставка в регіони";
include("./config.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>automafia.com.ua</title>
</head>
<body>

<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include(PATH_INFO."/block/left_block.php");
        ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include(PATH_INFO."/block/header_head.php");
        ?>
        <div  class = "content border">
            <p class = "redText h5smart">
                Розсилку товару здійснюємо переважно транспортними компаніями:
                </p>
            <ul class = "blueText ">
            <li><p class="h5smart">MEEST EXPRESS </p></a></li>
                <li><a href="https://www.meest-express.com.ua"><p class="h6smart">https://www.meest-express.com.ua</p></a></li>
            <li><p class="h5smart">ГЮНСЕЛ </p></a></li>
                <li><a href="http://www.gunsel.ua"><p class="h6smart">http://www.gunsel.ua</p></a></li>
            <li><p class="h5smart">НОВА ПОШТА</p></a></li>
                <li><a href="http://novaposhta.ua"><p class="h6smart">http://novaposhta.ua</p></a></li>
            <li><p class="h5smart">НІЧНИЙ ЕКСПРЕС</p></a></li>
                <li><a href="http://www.nexpress.com.ua"><p class="h6smart">http://www.nexpress.com.ua</p></a></li>
            <li><p class="h5smart">САТ</p></a></li>
                <li><a href="http://www.sat-trans.com"><p class="h6smart">http://www.sat-trans.com</p></a></li>
            </ul>

            <p class = "redText h5smart">   Здійснюємо доставку по Харкову. </p>

        </div>
        <div  class = "footer  border">
            <?php
            include(PATH_INFO."/block/footer.php")
            ?>
        </div>
    </div>
</div>

