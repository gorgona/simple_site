<?php
include("./config.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>


    <meta charset="UTF-8">
    <title>automafia.com.ua</title>
</head>
<body>
<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include(PATH_INFO."/block/left_block.php");
        ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        ?>
        <div  class = "content border">
           <p class="h6smart"> Пропонуємо роздрібним і оптовим клієнтам амортизатори ABESTA прямо з нашого складу в Харкові.
               В наявності широкий асортимент амортизаторів на японські корейські і європейські автомобілі.
               Висока якість амортизаторів ABESTA підтверджено багаторічною успішною експлуатацією виробів на дорогах України.
           </p>

            <p class="h6smart"> Важливо, що дані амортизатори мають оптимальне співвідношення параметрів ціна / якість,
                що вигідно як кінцевого споживача, так і магазинам і СТО пропонує ці амортизатори в роздріб.</p>


            <p class="h6smart"> На амортизатори ABESTA поширюється офіційна гарантія.</p>

            <p class="h6smart">УВАГА!!!</p>

            <p class="h6smart">Додатково, завжди в наявності комплекти пильовиків + відбійників на будь-який амортизатор.</p>

            <p class="h6smart"> SPO12770060 для діаметра штока 12-16мм (+SPO12)</p>

            <p class="h6smart"> SPO18770045 для діаметра штока 18мм</p>

            <p class="h6smart">SPO20770042 для діаметра штока 20мм</p>

            <p class="h6smart"> SPO22770046 для діаметра штока 22мм</p>


        </div>
        <div  class = "footer  border">
          <?php  include(PATH_INFO."/block/footer.php"); ?>
        </div>
    </div>


</div>

