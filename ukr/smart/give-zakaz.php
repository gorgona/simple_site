<?php
$page_name = "Як отримати замовлення";
include("./config.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>automafia.com.ua</title>
</head>
<body>
<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include(PATH_INFO."/block/left_block.php");
        ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include(PATH_INFO."/block/header_head.php");

        ?>
        <div  class = "content border">
            <table>
                <tr>
                    <td class="left-symbol"> <h1 class = "redText h1smart" >!</h1></td>
                    <td class="right-text">
                        <h3 class = "redText h6smart" >ШАНОВНІ ПОКУПЦІ!</h3>
                        <h3 class = "redText h6smart" > ПРИ ОТРИМАННЯ ТОВАРУ ОБОВ'ЯЗКОВО ЗВІРЯЙТЕ НОМЕРИ ЗАПЧАСТИН ТА ЇХ КІЛЬКІСТЬ З ВАШОЮ ВИДАТКОВОЮ НАКЛАДНОЮ </h3>
                    </td>
                </tr>
            </table>

            <div >
            <a href="<?php echo(ADRES); ?>givezakaz/givenomer.php"><button type="button" class="btn-primary lagbtn" >Я ЗНАЮ НОМЕР СВОГО ЗАМОВЛЕННЯ</button></a>


            <a href="<?php echo(ADRES); ?>givezakaz/givewithoutnomer.php"><button type="button" class="btn-primary lagbtn">Я НЕ ЗНАЮ НОМЕР СВОГО ЗАМОВЛЕННЯ</button></a>
           </div>


        </div>

</div>
    <div  class = "footer  border">
        <?php
        include(PATH_INFO."/block/footer.php")
        ?>
    </div>
</div>
</body>

