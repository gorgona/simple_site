<?php
$page_name = "Як отримати замовлення";
include("../config.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>automafia.com.ua</title>
    <link href="../css/official.css" rel="stylesheet">
    <script type="text/javascript" src="../js/adaptive.js"></script>
</head>
<body>
<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include(PATH_INFO."/block/left_block.php");
        ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include(PATH_INFO."/block/header_head.php");
        ?>
        <div  class = "content border">
            <h1 class = "redText" >ШАНОВНІ ПОКУПЦІ! ПРИ ОТРИМАННЯ ТОВАРУ ОБОВ'ЯЗКОВО ЗВIРЯЙТЕ НОМЕРИ ЗАПЧАСТИН ТА ЇХ КІЛЬКІСТЬ З ВАШОЮ ВИДАТКОВОЮ НАКЛАДНОЮ </h1>



            <h1 class = "redText" >Для отримання замовлення без номера замовлення подзвоните вашому менеджерові:</h1>

            <div >
                <?php
                include(PATH_INFO."/block/managerTable.php");
                ?>
            </div>
            <a href="<?php echo(ADRES); ?>give-zakaz.php"><button type="button" class="btn btn-primary btn-lg lagbtn"><h1>НАЗАД</h1></button></a>
        </div>
        <div  class = "footer  border">

        </div>
    </div>


</div>

</body>