<?php
$page_name = "Доставка в регіони";
include("./config.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>automafia.com.ua</title>
</head>
<body>

<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include(PATH_INFO."/block/left_block.php");
        ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include(PATH_INFO."/block/header_head.php");
        ?>
        <div  class = "content border">
            <p class = "redText">
                Розсилку товару здійснюємо переважно транспортними компаніями:
                </p>
            <ul class = "blueText">
            <li><a href="https://www.meest-express.com.ua">MEEST EXPRESS     https://www.meest-express.com.ua</a></li>
            <li><a href="http://www.gunsel.ua">ГЮНСЕЛ     http://www.gunsel.ua</a></li>
            <li><a href="http://novaposhta.ua">НОВА ПОШТА     http://novaposhta.ua</a></li>
            <li><a href="http://www.nexpress.com.ua">НІЧНИЙ ЕКСПРЕС     http://www.nexpress.com.ua</a></li>
            <li><a href="http://www.sat-trans.com">САТ     http://www.sat-trans.com</a></li>
            </ul>

            <p class = "redText">  Здійснюємо доставку по Харкову. </p>


        </div>
        <div  class = "footer  border">
            <?php
            include(PATH_INFO."/block/footer.php")
            ?>
        </div>
    </div>


</div>

