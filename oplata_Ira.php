<?php
$page_name = "Оплата";
include("./config.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <title>automafia.com.ua</title>

</head>
<body>
<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include(PATH_INFO."/block/left_block.php");
        ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include(PATH_INFO."/block/header_head.php");
        ?>
        <div  class = "content border">
            <h3 class="redText">Безналичный расчёт (UAH)</h3>

           <ul>
               <li class = "greenText">ПАТ КБ ПРИВАТ БАНК</li>
               <li>р/с UA353515330000026001052205729</li>
               <li>МФО: 351533 </li>
               <li> ЕДРПОУ: 2827021122 </li>
               <li style="font-weight: bolder"> Владелец счёта: ФЛП Лысенко Ирина Игоревна </li>
           </ul>
           <a href="<?php echo(PATH_INFO); ?>/pravila.php"><h3 class = "violetText">ОТВЕТСТВЕННОСТЬ</h3></a>

        </div>
        <div  class = "footer  border">
            <?php
            include(PATH_INFO."/block/footer.php")
            ?>
        </div>
    </div>


</div>

