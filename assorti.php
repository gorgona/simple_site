<?php
$page_name = "Ассортимент";
include("./config.php");

?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <title>automafia.com.ua</title>
</head>
<body>
<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include(PATH_INFO."/block/left_block.php");
        ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include(PATH_INFO."/block/header_head.php");
        ?>
        <div  class = "content border centerAlign">
           <p class = "redText">
            Компания АВТОМАФИЯ (automafia), предлагает запчасти практически для любых автомобилей иностранного производства
           </p>

         <div  class = "footer  border">
          <?php
          include(PATH_INFO."/block/footer.php")
          ?>
         </div>

            <p class = "bolder>Автозапчасти ACURA</p>
            <p>CDX, MDX, NSX, RDX, RLX, TLX, TL, INTEGRA, ZDX.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/alfa_romeo.jpg">
            <p class = "bolder centerAlign ">Автозапчасти ALFA ROMEO</p>
            <p class = "centerAlign">33, 75, 145, 146, 147, 155, 156, 164, 166, 4C, 8C COMPETIZIONE, 8C SPIDER, BRERA, GIULIA, GIULIETTA, GT, MITO, SPIDER, STELVIO.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/audi.jpg">
            <p class = "bolder centerAlign">Автозапчасти AUDI</p>
            <p class = "centerAlign">80, 90, 100, A1, A2, A3, A4, A4 ALLROAD QUATTRO,A5, A6, A6 SDN / AVANT, A6 ALLROAD  QUATTRO, A7,A8, Q2, Q3, Q5, Q7, R8, RS Q3, RS3, RS4, RS5, RS6, RS7, S3, S4, S5, S6, S7, S8, SQ5, SQ7, TT, TT OFFROAD, TT RS, TTS.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/bmw.gif">
            <p class = "bolder centerAlign">Автозапчасти BMW</p>
            <p class = "centerAlign">1 SERIES, 2 SERIES, 3 SERIES, 3 SERIES GT, 4 SERIES, 5 SERIES, 5 SERIES GT,  6 SERIES, 7 SERIES, I3, I8, M1, M3, M4, M6, M6, M6 GRAN COUPE, X1, X2, X3, X4, X5, X5 M, X6, X6 M, Z3, Z4.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/chery.gif">
            <p class = "bolder centerAlign">Автозапчасти CHERY</p>
            <p class = "centerAlign">AMULET (A15), ARRIZO 7, BONUS, BONUS 3, ELARA (A21), FORA, INDIS, KIMO (A1), M11, ORIENTAL SON, QQ (S11), QQ6, QQ JAGGI (S21), TIGGO (T11), EASTAR (B11), CROSS EASTAR (B14), KARRY (A18), VERY.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/chevrolet.gif">
            <p class = "bolder centerAlign">Автозапчасти CHEVROLET</p>
            <p class = "centerAlign">AVEO, ALERO, CAMARO, CAPTIVA, COBALT, CORVETTE, CRUZE, EPICA, IMPALA, EVANDA, LACETTI HB, LACETTI SDN / COMBI, LANOS, LUMINA, MALIBU, NIVA, ORLANDO, REZZO, SILVERADO, SPARK, SS, TAHOE, TACUMA, TRACKER, TRAILBLAZER, TRAVERSE, VOLT.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/chrysler.gif">
            <p class = "bolder centerAlign">Автозапчасти CHRYSLER</p>
            <p class = "centerAlign">200, 300C, 300M, CROSSFIRE, GRAND VOYAGER, NEON, PACIFICA, STRATUS, VOYAGER, PT CRUISER, SEBRING.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/citroen.gif">
            <p class = "bolder centerAlign">Автозапчасти CITROEN</p>
            <p class = "centerAlign">AX, BX, ZX, BERLINGO, C15, C1, C2, C25, C3, C4, C4 PICASSO, C5, JUMPER, C3 PICASSO, C4 AIRCROSS, C4 CACTUS, C4 GRAND PICASSO, C4 SEDAN, C5 AIRCROSS, C6, C-CROSSER, C-ELYSEE, SPACETOURER, JUMPY, XANTIA, XSARA,
            XSARA PICASSO.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/dacia.gif">
            <p class = "bolder centerAlign">Автозапчасти DACIA</p>
            <p class = "centerAlign">LOGAN, SOLENZA, NOVA, SANDERO, DUSTER, LOGAN MCV, LODGY STEPWAY, DOKKER, DUSTRUCK.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/dadi.gif">
            <p class = "bolder centerAlign">Автозапчасти DADI</p>
            <p class = "centerAlign">CITY LEADING, NEW SMOOTHING, SHUTLE.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/daewoo.gif">
            <p class = "bolder centerAlign">Автозапчасти DAEWOO</p>
            <p class = "centerAlign">ESPERO, LANOS, LEGANZA, MATIZ, NEXIA, NUBIRA, GENTRA, TACUMA, LACETTI, MUSSO, TOSCA, KALOS, RACER, DAMAS, KORANDO, REZZO, TICO, WINSTORM.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/fiat.jpg">
            <p class = "bolder centerAlign">Автозапчасти FIAT</p>
            <p class = "centerAlign">BRAVA, 124 SPIDER, 500, 500L, 500S, 500X, ALBEA, ARGO,  BRAVO, CROMA, CINQUECENTO, COUPE, DOBLO, DUCATO, DUNA, IDEA, FREEMONT, FULLBACK, GRANDE PUNTO, LINEA, PANDA, TORO, FIORINO, MAREA, MULTIPLA, PALIO, PUNTO, SCUDO, STILO, TEMPRA, TIPO, UNO.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/ford.jpg">
            <p class = "bolder centerAlign">Автозапчасти FORD</p>
            <p class = "centerAlign">ESCORT, ORION, FIESTA, COURIER, FOCUS, FOCUS C-MAX, S-MAX, FUSION, GALAXY, KA, MAVERICK, MONDEO, SCORPIO, SIERRA, TRANSIT, TRANSIT CONNECT, ECOSPORT, EDGE, ESCAPE, EXPEDITION, EXPLORER, F-150, GRAND C-MAX, KA+, KUGA, MAVERICK, MUSTANG, RANGER, TAURUS.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/geely.jpg">
            <p class = "bolder centerAlign">Автозапчасти GEELY</p>
            <p class = "centerAlign">CK, MK, EMGRAND, EMGRAND EC7, EMGRAND GT, EMGRAND X7, GC6, MK CROSS, OTAKA, VISION.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/great_wall.gif">
            <p class = "bolder centerAlign">Автозапчасти GREAT WALL</p>
            <p class = "centerAlign">HOVER, COOLBEAR, COWRY, DEER, FLORID, PERI, WINGLE.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/honda.gif">
            <p class = "bolder centerAlign">Автозапчасти HONDA</p>
            <p class = "centerAlign">ACCORD, CIVIC, CROSSTOUR, CR-V, JAZZ, PRELUDE, SHUTTLE/ODYSSEY, FIT, FR-V, HR-V, LEGEND, PILOT, STREAM, INSIGHT, N-ONE, FREED HYBRID, ODYSSEY, STEPWGN, CLARITY, GRACE, JADE.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/hyundai.gif">
            <p class = "bolder centerAlign">Автозапчасти HYUNDAI</p>
            <p class = "centerAlign">ACCENT, ATOS, GETZ, H-1/H200, H-1, H-100,
            H-250, LANTRA, MATRIX, PONY, SANTA FE, SONATA, TUCSON, COUPE, CRETA, ELANTRA, EQUUS, GENESIS, GRAND SANTA FE, GRANDEUR, I10, I20, I30, I40, IONIQ, IX25, IX35, KONA, MATRIX, NF SONATA, SOLARIS, STAREX, TERRACAN, VELOSTER, VERNA.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/iveco.gif">
            <p class = "bolder centerAlign">Автозапчасти IVECO</p>
            <p class = "centerAlign">DAILY.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/jeep.gif">
            <p class = "bolder centerAlign">Автозапчасти JEEP</p>
            <p class = "centerAlign">CHEROKEE, COMMANDER, COMPASS, GRAND CHEROKEE, LIBERTY, RENEGADE, WRANGLER.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/kia.gif">
            <p class = "bolder centerAlign">Автозапчасти KIA</p>
            <p class = "centerAlign">CARNIVAL, CADENZA, CARENS, CEED, CERATO, CLARUS, K2 CROSS, MAGENTIS, MOHAVE, NIRO, OPIRUS, OPTIMA, QUORIS, PICANTO, RIO, SEPHIA, SHUMA, SOUL, SPECTRA, SORENTO JEEP, SPORTAGE JEEP, STINGER, STONIC, VENGA, OPIRUS.</p>


            <img src="<?php echo(ADRES) ; ?>/images/assorti/lancia.gif">
            <p class = "bolder centerAlign">Автозапчасти LANCIA</p>
            <p class = "centerAlign">DEDRA, THEMA, DELTA INTEGRALE, LYBRA, MUSA, PHEDRA, THESIS, YPSILON, ZETA.</p>


            <img src="<?php echo(ADRES) ; ?>/images/assorti/lexus.gif">
            <p class = "bolder centerAlign">Автозапчасти LEXUS</p>
            <p class = "centerAlign">LX, RX, CT, ES, GS, GSF, GX, IS, LC, LFA, LS, NX, RC, RC F, SC.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/mazda.gif">
            <p class = "bolder centerAlign">Автозапчасти MAZDA</p>
            <p class = "centerAlign">121, 121, 2, 3, 323, 5, 6, 626, BT-50, BUS 1800-3000, MPV, MX-5, PREMACY, PICK-UP, CX-3, CX-4, CX-5, CX-7, CX-9, RX-8, TRIBUTE.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/mersedes.gif">
            <p class = "bolder centerAlign">Автозапчасти MERCEDES</p>
            <p class = "centerAlign">W123,W124,S124,C124,W126,C126,C140 (S-CLASS), 168 (A-CLASS), W169,W176, AMG GT, AMG GTR, W245,W246 (B-CLASS),
            SLK R171, W190,W201,W202,S202,W203,S203,CL203, W204,S204,W205,S205 (C-CLASS), CLA, (CLC-CLASS), C208,C209 (CLK-CLASS), C215,C216 (CL-CLASS), W218,W219 (CLS-CLASS), W210,S210,W211,S211,W212,W212,C207,A207,W213,S213,C208,A208 (E-CLASS) ALL-TERRAIN, W463,W463F (G-CLASS), (GLA-CLASS), GLC, X164, X166 (GL-CLASS), GLE, X204 (GLK-CLASS), GLS, W163,W164,W166 (M-CLASS), W251 (R-CLASS),  W114,W115,W140,W220,W221,W222 (S-CLASS), SLC, R230,R231 (SL-CLASS), R170,R171,R172 (SLK-CLASS), SLS AMG, BUS (207-410), BUS (406-613), BUS (507-814), BUS (512-814), BUS (MB100), BUS SPRINTER, BUS VITO (V-CLASS),
            BUS VIANO / VITO, BUS VANEO, CITAN,</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/mitsubishi.jpg">
            <p class = "bolder centerAlign">Автозапчасти MITSUBISHI</p>
            <p class = "centerAlign">CARISMA, COLT, ECLIPSE, ECLIPSE CROSS, GALANT, GRANDIS, I-MIEV, L200, L 400, LANCER, LANCER X, OUTLANDER, OUTLANDER XL, PAJERO, PAJERO SPORT, SPACE GEAR, SPACE WAGON/RUNNER, ASX, SPACE STAR.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/nissan.gif">
            <p class = "bolder centerAlign">Автозапчасти NISSAN</p>
            <p class = "centerAlign">ALMERA (G11,N15,N16,N17), ALMERA CLASSIC, CUBE, GT-R, JUKE (YF15), KICKS, LEAF, MAXIMA (J30,A32,A33), MICRA (K11,K12,K13), MURANO, NAVARA, NOTE, PATROL (160,GR60 Y60),  PATHFINDER, PRIMERA (P10,W10,P11,W11,P12,W12), PULSAR, QASHQAI, SENTRA, SERENA, SYLPHY, VANETTE, SUNNY (B12,N13,N14,Y10), TEANA, TERRANO I, TERRANO II, TIIDA, X-TRAIL, 350Z, 370Z.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/opel.gif">
            <p class = "bolder centerAlign">Автозапчасти OPEL</p>
            <p class = "centerAlign">ADAM, AGILA, AMPERA, ANTARA, ASCONA, ASTRA (F,G,H), CALIBRA, COMBO, CORSA, CROSSLAND X, FRONTERA, GRANDLAND X, GT, INSIGNIA, KARL, KADETT, MASCOTT, MERIVA, MOKKA, MONTREY, MOVANO, SIGNUM, OMEGA, RECORD, SENATOR, TIGRA,VECTRA, VIVARO, ZAFIRA.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/peugeot.gif">
            <p class = "bolder centerAlign">Автозапчасти PEUGEOT</p>
            <p class = "centerAlign">107, 1007, 108, 2008, 206, 207, 208, 301, 306, 307, 308, 3008, 309, 4007, 4008, 405, 406, 407, 408, 5008, 508, 605, 607,
            806, 807, BOXER, EXPERT, J5, PARTNER, RCZ, TRAVELLER.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/porsche.jpg">
            <p class = "bolder centerAlign">Автозапчасти PORSCHE</p>
            <p class = "centerAlign">CAYENNE, 718, 911, 918 SPYDER, BOXSTER, CAYMAN, MACAN, PANAMERA.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/renault.gif">
            <p class = "bolder centerAlign">Автозапчасти RENAULT</p>
            <p class = "centerAlign">CLIO, ALASKAN, DUSTER, FLUENCE, GRAND SCENIC, KADJAR, KAPTUR, KOLEOS, KWID, SYMBOL, ESPACE, EXPRESS, RAPID, KANGOO, LAGUNA, LATITUDE, LOGAN, MASTER, MEGANE, SANDERO, SYMBOL, TALISMAN, VEL SATIS, SCENIC, R9, R11,
            R19, R21, R21, R25, SAFRANE, TRAFIC,
            TWINGO.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/rover.gif">
            <p class = "bolder centerAlign">Автозапчасти ROVER</p>
            <p class = "centerAlign">25, 45, 75, 100, 200, 200/400, 200, 400, 600.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/saab.jpg">
            <p class = "bolder centerAlign">Автозапчасти SAAB</p>
            <p class = "centerAlign">900, 9000 CC/CD CS/AERO, 9-3, 9-5.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/seat.gif">
            <p class = "bolder centerAlign">Автозапчасти SEAT</p>
            <p class = "centerAlign">ALHAMBRA, ALTEA, ALTEA FREETRACK, ALTEA XL, ATECA FR, AROSA, EXEO, IBIZA, IBIZA FR,  CORDOBA, INCA, LEON, LEON CUPRA, LEON FR,  MALAGA, MARBELA, TOLEDO, TOLEDO.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/skoda.gif">
            <p class = "bolder centerAlign">Автозапчасти SKODA</p>
            <p class = "centerAlign">FABIA, CITIGO, FABIA RS, FABIA SCOUT, KAROQ, KODIAQ, FAVORIT, FELICIA, OCTAVIA, OCTAVIA RS, OCTAVIA SCOUT, RAPID, ROOMSTER, SUPERB, YETI.</p>


            <img src="<?php echo(ADRES) ; ?>/images/assorti/soueast.gif">
            <p class = "bolder centerAlign">Автозапчасти SOUEAST</p>
            <p class = "centerAlign">LIONCEL, DELICA, FREECA, LANDIO, V3, VERYCA.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/subaru.gif">
            <p class = "bolder centerAlign">Автозапчасти SUBARU</p>
            <p class = "centerAlign">BRZ, FORESTER, IMPREZA, IMPREZA WRX, JUSTY, LEGACY, OUTBACK, TRIBECA, LEONE, WRX, XV.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/suzuki.gif">
            <p class = "bolder centerAlign">Автозапчасти SUZUKI</p>
            <p class = "centerAlign">ALTO, BALENO, CELERIO, KIZASHI, IGNIS, SWIFT,
            VITARA SHORT, VITARA GRAND, WAGON R+,</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/toyota.gif">
            <p class = "bolder centerAlign">Автозапчасти TOYOTA</p>
            <p class = "centerAlign">4-RUNNER, ALPHARD, AURIS, AVALON, AVENSIS,
            AYGO, CAMRY,
            CARINA, COROLLA, CELICA, C-HR, FJ CRUISER, FORTUNER, GT86, HARRIER, HIGHLANDER, IQ,
            CROWN, HI-ACE, GRANIVA, HI-LUX, LANDCRUISER, LANDCRUISER  PRADO, PRIUS,
            PICNIC, PREVIA, RAV4, SIENNA, SIENTA, VENZA, VERSO, VERSO-S, VIOS, STARLET, TERCEL, YARIS .</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/volvo.gif">
            <p class = "bolder centerAlign">Автозапчасти VOLVO</p>
            <p class = "centerAlign">240/260, 340/360, 440/460, 740, 760, 840/850,
            940, 960, S40/V40, S60, S70/V70, S80, C30, C70,  S60 CROSS COUTRY, S90, V40, V40 CROSS COUNTRY, V50, V60, V60 CROSS COUNTRY, V70, V90, V90 CROSS COUNTRY, XC40, XC60, XC70, XC90.</p>

            <img src="<?php echo(ADRES) ; ?>/images/assorti/volksvagen.gif">
            <p class = "bolder centerAlign">Автозапчасти VOLKSWAGEN</p>
            <p class = "centerAlign">AMAROK, ARTEON, ATLAS, BEETLE, BORA, CADDY, CROSS GOLF, CROSS POLO, CRAFTER, FOX, GOLF 2, GOLF 3,
            GOLF 4, GOLF 5, GOLF ALLTRACK, GOLF GTI, GOLF PLUS, GOLF R,  JETTA 2, JETTA, LT I, LT II, LT III,
            LUPO, NEW BEETLE, PASSAT B2, PASSAT B3, PASSAT B4,
            PASSAT B5, PASSAT B7, PASSAT B6, PASSAT ALLTRACK, PASSAT CC, PASSAT GTE, PHAETON, POLO 3, POLO 4, POLO 5, POLO 6, POLO SEDAN, SHARAN, SCIROCCO,
            T3, T4, T4 CARAVELLE/MULTIVAN, T5, TOUAREG, TOURAN, TIGUAN, VENTO, TRANSPORTER.</p>



        </div>

    </div>


</div>
</body>

