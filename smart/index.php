<?php
$page_name = "Главная ";
include("./config.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>automafia.com.ua</title>
</head>

<body>

<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include(PATH_INFO."/block/left_block.php"); ?>
    </div>

    <div id = "right" class = "right border">
        <div>
            <?php
            include(PATH_INFO."/block/header.php");
            include(PATH_INFO."/block/header_head.php");
            ?>
        </div>

        <div  class = "content border">
            <?php
              include(PATH_INFO."/block/textContact.php");
              include(PATH_INFO."/block/sheduler.php");
              include("./block/footer.php");
            ?>
        </div>
    </div>
</div>
</body>

