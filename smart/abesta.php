<?php
    $page_name = "ABESTA";
    include("./config.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>automafia.com.ua</title>
</head>
<body>
<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
       include(PATH_INFO."/block/left_block.php");
       ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include (PATH_INFO."/block/header_head.php");
        ?>

        <div  class = "content border">
        <p class="h6smart"> Предлагаем розничным и оптовым клиентам амортизаторы
           <span style = "color: red; font-style: italic ; font-weight: bolder; font-size: larger ">
            ABESTA
                 </span>
           прямо с нашего склада в Харькове.
           В наличии широкий ассортимент амортизаторов на японские корейские и европейские автомобили.
           Высокое качество амортизаторов
            <span style = "color: red; font-style: italic ; font-weight: bolder; font-size: larger ">
            ABESTA
                 </span>
           подтверждено многолетней успешной эксплуатацией изделий на дорогах Украины.
           Немаловажно, что данные амортизаторы имеют оптимальное соотношения параметров цена/качество, что выгодно как конечному потребителю,
           так и магазинам и СТО предлагающим эти амортизаторы в розницу.
        </p>

        <p class="h6smart">
            На амортизаторы
            <span style = "color: red; font-style: italic ; font-weight: bolder; font-size: larger ">
            ABESTA
                 </span>
            распространяется официальная гарантия.
        </p>

        <p class="h6smart">
            <span style = "color: red;  font-weight: bolder; font-size: larger ">
            ВНИМАНИЕ!!!
            </span>
        </p>
        <p class="h6smart">
            Дополнительно, всегда в наличии комплекты пыльников+отбойников на любой амортизатор.
            <ul>
                <li> <p class="h6smart">SPO12770060 для диаметра штока 12-16мм (+SPO12)</p></li>
                <li> <p class="h6smart">SPO18770045 для диаметра штока 18мм</p></li>
                <li> <p class="h6smart">SPO20770042 для диаметра штока 20мм</p></li>
                <li> <p class="h6smart">SPO22770046 для диаметра штока 22мм</p></li>
            </ul>
        </p>



        </div>
        <div  class = "footer  border">
            <?php
            include(PATH_INFO."/block/footer.php")
            ?>
        </div>
    </div>


</div>
</body>
