        <p>
            <span style = "color: red;  font-weight: bolder;  "> ВНИМАНИЕ! </span> Приоритетными являются и соответственно быстрее
            обрабатываются запросы поступающие на мобильные телефоны и Viber
            сотрудников компании АВТОМАФИЯ (automafia)!
            <a href = "pravila.php" class = "blueText" > ОТВЕТСТВЕННОСТЬ </a>
        </p>
        <p>
            <span style = "color: red;  font-weight: bolder; "> ВНИМАНИЕ! </span> Уважаемые клиенты, если вы хотите обслуживаться
            не у закреплённого за вами менеджера, а хотите сменить его на другого более подходящего вам, по той или иной причине
            (по любой причине), оповещаем вас о том что такая возможность есть. Для этого вам нужно связаться с
            Виталием Панасенко или Малышевым Александром. Они уполномочены урегулировать данный вопрос.
        </p>

        <div class = "greenText blockMargineLeft5"  >
            <h3 style="margin-left: 50px;" >Время работы офиса:</h3>
            <table class = "tableRaspisanie" id = "WorkSheduler">
                <tr>
                    <td>Понедельник - Пятница</td>
                    <td> 10:00 - 18:00</td>
                </tr>
                <tr>
                    <td>Суббота </td>
                    <td>10:00 - 15:00</td>
                </tr>
                <tr>
                    <td>Воскресенье </td>
                    <td>выходной </td>
                </tr>
            </table>
        </div>

        <table id = "Manager">
						<tr>
                            <td>067-570-92-11</td>
                            <td>‎093-761-75-25</td>
                            <td><span class="violetText" >‎050-325-75-52 +Viber</span></td>
                            <td>Артур Корецкий</td>
                            <td>u0017@automafia.com.ua</td>
                        </tr>
                        <tr>
                            <td>067-579-31-42</td>
                            <td>063-761-39-17</td>
                            <td><span class="violetText" >050-300-39-22 +Viber</span></td>
                            <td>Юрий Маренич</td>
                            <td>u0016@automafia.com.ua</td>
                        </tr>

                        <tr>
                            <td>067-579-31-49</td>
                            <td>093-761-75-29</td>
                            <td><span class="violetText" >050-327-90-86 +Viber</span></td>
                            <td>Алексей Шатный</td>
                            <td>u0015@automafia.com.ua</td>
                        </tr>
                        <tr>
                            <td>067-546-23-24 </td>
                            <td>093-761-75-22  </td>
                            <td><span class="violetText" >050-327-90-34 +Viber </span> </td>
                            <td> Валерий Мудренко</td>
                            <td>u0012@automafia.com.ua </td>
                        </tr>

                        <tr>
                            <td>067-560-21-88</td>
                            <td>093-761-75-18</td>
                            <td>050-325-29-23 +Viber</td>
                            <td><span class="violetText" >Александр Малышев</span></td>
                            <td>u0011@automafia.com.ua</td>
                        </tr>


                        <tr>
                            <td>067-579-31-60</td>
                            <td>093-761-75-23</td>
                            <td><span class="violetText">050-327-89-23 +Viber </span></td>
                            <td> Александр Суслов</td>
                            <td>u0010@automafia.com.ua</td>
                        </tr>
                        <tr>
                            <td>067-579-31-38</td>
                            <td>093-761-75-21</td>
                            <td><span class="violetText">050-327-54-06 +Viber</span></td>
                            <td>Владимир Раскошный</td>
                            <td>u0009@automafia.com.ua </td>
                        </tr>

                        <tr>
                            <td>067-579-31-59</td>
                            <td>093-761-39-18</td>
                            <td><span class="violetText">050-325-00-66 +Viber</span></td>
                            <td>Юрий Карбовский</td>
                            <td>u0008@automafia.com.ua</td>
                        </tr>
                        <tr>
                            <td>067-579-31-36</td>
                            <td>063-761-24-73</td>
                            <td><span class="violetText">050-400-55-74 +Viber</span></td>
                            <td>Вячеслав Москвин</td>
                            <td>u0007@automafia.com.ua</td>
                        </tr>

                      
                                    <tr>
                                        <td>067-579-31-16</td>
                                        <td>063-761-39-16</td>
                                        <td><span class="violetText">050-325-13-25 +Viber</span></td>
                                        <td>Андрей Сурков</td>
                                        <td>u0006@automafia.com.ua</td>
                                    </tr>

                                    <tr>
                                        <td>097-328-78-48</td>
                                        <td>063-761-39-15</td>
                                        <td><span class="violetText">050-325-35-19 +Viber</span></td>
                                        <td>Виталий Иващенко</td>
                                        <td>u0005@automafia.com.ua</td>
                                    </tr>

                                    <tr>
                                        <td>067-579-08-29</td>
                                        <td>063-761-39-14</td>
                                        <td><span class="violetText">050-325-29-30 +Viber</span></td>
                                        <td> Сергей Ващенко </td>
                                        <td>u0004@automafia.com.ua</td>
                                     </tr>


                                    <tr>
                                        <td>067-579-31-17</td>
                                        <td>063-761-39-13</td>
                                        <td><span class="violetText">050-346-39-36 +Viber</span></td>
                                        <td>Константин Шетилов</td>
                                        <td>u0003@automafia.com.ua</td>
                                    </tr>

                                    <tr>
                                        <td>067-579-31-24</td>
                                        <td>063-761-39-12</td>
                                        <td><span class="violetText">050-325-29-38 +Viber</span></td>
                                        <td>Виктор Боровой</td>
                                        <td>u0002@automafia.com.ua</td>
                                    </tr>

                                    <tr>
                                        <td>067-579-31-28</td>
                                        <td>063-761-39-11</td>
                                        <td><span class="violetText">050-325-29-37 +Viber</span></td>
                                        <td>Вячеслав Пашкевич</td>
                                        <td>u0001@automafia.com.ua</td>
                                    </tr>

           

        </table>




        </br>
<span class = "greenText">
    <h3>Контакты с поставщиками и партнерами:</h3>
</span>

<span class = "blueText" >
    Виктор Боровой
</span>
        </br>
        0675793124,   0637613912, <span class = "violetText"> 0503252938 +Viber </span>
        </br>
        e-mail: u0002@automafia.com.ua
        </br>
        </br>

        </br>
<span class = "greenText">
     <h3>Информация по возвратам и страхование:</h3>
</span>

<span class = "blueText" >
    Александр Малышев
</span>
        </br>
        0675602188,   0637617518,  <span class = "violetText"> 0503252923 +Viber </span>
        </br>
        e-mail: u0011@automafia.com.ua
        </br>
        </br>

        </br>
<span class = "greenText">
    <h3> Информация по декларациям, складу, текущему наличию товара:</h3>
</span>
<span class = "blueText" >
   Виталий Иващенко
</span>
        </br>
        0973287848,   0637613915,  <span class = "violetText">  0503253519 +Viber </span>
        </br>
        e-mail: u0005@automafia.com.ua
        </br>
 <span class = "blueText" >
  Александр Суслов
</span>
        </br>
        0675793160,   0937617523,  <span class = "violetText">  0503278923 +Viber </span>
        </br>
        e-mail: u0010@automafia.com.ua
        </br>
 <span class = "blueText" >
  Валерий Мудренко
</span>
        </br>
        0675462324,   0937617522,  <span class = "violetText">  0503279034 +Viber </span>
        </br>
        e-mail: u0012@automafia.com.ua
        </br>
        </br>


<span class = "greenText">
    <h3>Водитель:</h3>
</span>
<span class = "blueText" >
  Владимир Васильевич
</span>
        </br>
        0662065396
        </br>
        </br>

<span class = "greenText">
   <h3>Организационные вопросы:</h3>
</span>
<span class = "blueText" >
  Лысенко Олег Владимирович
</span>
<span class = "violetText">
</br>
    0973019922 +Viber
    </br>
    0633019922 +Viber
    </br>
    0503019922 +Viber
    </br>
    e-mail: automafia@gmail.com
    </br>
    </br>
</span>

<span class = "greenText">
   <h3> АВТОМОЙКА </h3>
</span>
   <span class = "blueText" >
  Куплевацкий Антон
</span>
        </br>
        0639324544
        </br>
        </br>

<span class = "greenText">
   <h3>СТО:</h3>
</span>
<span class = "blueText" >
Галаван Владислав Зиновьевич
</span>
<span class = "violetText">
</br>
    0679385906 +Viber
    </span>
        </br>
        Ремонт двигателя, ходовой, кузовной цех, развал.
        </br>
    <span class="redText">
    Покупателям автомагазина скидка на установку.
   </span>
        </br>







