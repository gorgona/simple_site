﻿<div style = " margin-left:0%">
<li class="mainmenu"><p class="h5smart">Сергей Желавский</p>
    <ul >
        <li>
            <a href="tel:+380677915033"><p class="h6smart">(067)791-50-33</p></a>
        </li>
        <li>
            <a href="tel:+380932903552"><p class="h6smart">(093)290-35-52</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503253538"><p class="h6smart">(050)325-35-38 +Viber</p></a></span>
        </li>
        <li>
            <p class="h6smart">u0027@automafia.com.ua</p>
        </li>
    </ul>
</li>

<li class="mainmenu"><p class="h5smart">Менеджер 26</p>
    <ul >
        <li>
            <a href="tel:+380677913705"><p class="h6smart">(067)791-37-05</p></a>
        </li>
        <li>
            <a href="tel:+380932903551"><p class="h6smart">(093)290-35-51</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503253537"><p class="h6smart">(050)325-35-37 +Viber</p></a></span>
        </li>
        <li>
            <p class="h6smart"> u0026@automafia.com.ua</p>
        </li>
    </ul>
</li>

<li class="mainmenu"><p class="h5smart">Менеджер 25</p>
    <ul >
        <li>
            <a href="tel:+380677914086"><p class="h6smart">(067)791-40-86</p></a>
        </li>
        <li>
            <a href="tel:+380930853036"><p class="h6smart">(093)085-30-36</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503253501"><p class="h6smart">(050)325-35-01 +Viber</p></a></span>
        </li>
        <li>
            <p class="h6smart">u0025@automafia.com.ua</p>
        </li>
    </ul>
</li>

<li class="mainmenu"><p class="h5smart">Менеджер 24</p>
    <ul>
        <li>
            <a href="tel:+380677395028"><p class="h6smart">(067)739-50-28</p></a>
        </li>
        <li>
            <a href="tel:+380932881109"><p class="h6smart">(093)288-11-09</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503254959"><p class="h6smart">(050)325-49-59 +Viber</p></a></span>
        </li>
        <li>
            <p class="h6smart"> u0024@automafia.com.ua</p>
        </li>
    </ul>
</li>

<li class="mainmenu"><p class="h5smart">Алла Булгакова</p>
    <ul>
        <li>
            <a href="tel:+380675793124"><p class="h6smart">(067)579-31-24</p></a>
        </li>
        <li>
            <a href="tel:+380637613912"><p class="h6smart">(063)761-39-12</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503252938"><p class="h6smart">(050)325-29-38 +Viber</p></a></span>
        </li>
        <li>
            <p class="h6smart">u0018@automafia.com.ua</p>
        </li>
    </ul>
</li>

<li class="mainmenu"><p class="h5smart">Виктория Ройтих</p>
    <ul >
        <li>
            <a href="tel:+380675709211"><p class="h6smart">(067)570-92-11</p></a>
        </li>
        <li>
            <a href="tel:+380937617525"><p class="h6smart">(093)761-75-25</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503257552"><p class="h6smart">(050)325-75-52 +Viber</p></a></span>
        </li>
        <li>
            <p class="h6smart">u0017@automafia.com.ua</p>
        </li>
    </ul>
</li>

<li class="mainmenu"><p class="h5smart">Юрий Маренич</p>
    <ul >
        <li>
            <a href="tel:+380675793142"><p class="h6smart">(067)579-31-42</p></a>
        </li>
        <li>
            <a href="tel:+380637613917"><p class="h6smart">(063)761-39-17</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503003922"><p class="h6smart">(050)300-39-22 +Viber</p></a></span>
        </li>
        <li>
            <p class="h6smart">u0016@automafia.com.ua
        </li>
    </ul>
</li>

<li class="mainmenu"><p class="h5smart">Алексей Шатный</p>
    <ul >
        <li>
            <a href="tel:+380675793149"><p class="h6smart">(067)579-31-49</p></a>
        </li>
        <li>
            <a href="tel:+380937617529"><p class="h6smart">(093)761-75-29</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503279086"><p class="h6smart">(050)327-90-86 +Viber</p></a></span>
        </li>
        <li>
            <p class="h6smart">u0015@automafia.com.ua</p>
        </li>
    </ul>
</li>

<li class="mainmenu"><p class="h5smart">Олег Дудняк</p>
    <ul >
        <li>
            <a href="tel:+380675462324"><p class="h6smart">(067)546-23-24</p></a>
        </li>
        <li>
            <a href="tel:+380937617522"><p class="h6smart">(093)761-75-22</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503279034"><p class="h6smart">(050)327-90-34 +Viber</p></a></span>
        </li>
        <li>
            <p class="h6smart">u0012@automafia.com.ua</p>
        </li>
    </ul>
</li>

<li class="mainmenu"><p class="h5smart">Александр Малышев</p>
    <ul >
        <li>
            <a href="tel:+380675602188"><p class="h6smart">(067)560-21-88</p></a>
        </li>
        <li>
            <a href="tel:+380937617518"><p class="h6smart">(093)761-75-18</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503252923"><p class="h6smart">(050)325-29-23 +Viber</p></a></span>
        </li>
        <li>
            <p class="h6smart">u0011@automafia.com.ua</p>
        </li>
    </ul>
</li>

<li class="mainmenu"><p class="h5smart">Сергей Космаков</p>
    <ul>
        <li>
            <a href="tel:+380675793160"><p class="h6smart">(067)579-31-60</p></a>
        </li>
        <li>
            <a href="tel:+380937617523"><p class="h6smart">(093)761-75-23</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503278923"><p class="h6smart">(050)327-89-23 +Viber</p></a></span>
        </li>
        <li>
            <p class="h6smart"> u0010@automafia.com.ua</p>
        </li>
    </ul>
</li>

<li class="mainmenu"><p class="h5smart">Менеджер 9</p>
    <ul>
        <li>
            <a href="tel:+380675793138"><p class="h6smart">(067)579-31-38</p></a>
        </li>
        <li>
            <a href="tel:+380937617521"><p class="h6smart">(093)761-75-21</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503275406"><p class="h6smart">(050)327-54-06 +Viber</p></a></span>
        </li>
        <li>
            <p class="h6smart">u0009@automafia.com.ua</p>
        </li>
    </ul>
</li>

<li class="mainmenu"><p class="h5smart">Евгений Маслий</p>
    <ul >
        <li>
            <a href="tel:+380675793159"><p class="h6smart">(067)579-31-59</p></a>
        </li>
        <li>
            <a href="tel:+380637613918"><p class="h6smart">(063)761-39-18</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503250066"><p class="h6smart">(050)325-00-66 +Viber</p></a></span>
        </li>
        <li>
            <p class="h6smart">u0008@automafia.com.ua</p>
        </li>
    </ul>
</li>

<li class="mainmenu"><p class="h5smart">Александр Дудык</p>
    <ul >
        <li>
            <a href="tel:+38‎0675793136"><p class="h6smart">(067)570-92-69</p></a>
        </li>
        <li>
            <a href="tel:+380637612473"><p class="h6smart">(063)761-24-73</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503257551"><p class="h6smart">(050)325-75-51 +Viber</p></a></span>
        </li>
        <li>
            <p class="h6smart"> u0007@automafia.com.ua</p>
        </li>
    </ul>
</li>

<li class="mainmenu"><p class="h5smart">Андрей Сурков</p>
    <ul >
        <li>
            <a href="tel:+380675793116"><p class="h6smart">(067)579-31-16</p></a>
        </li>
        <li>
            <a href="tel:+380637613916"><p class="h6smart">(063)761-39-16</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503251325"><p class="h6smart">(050)325-13-25 +Viber</p></a></span>
        </li>
        <li>
            <p class="h6smart"> u0006@automafia.com.ua</p>
        </li>
    </ul>
</li>

<li class="mainmenu"><p class="h5smart">Менеджер 5</p>
    <ul >
        <li>
            <a href="tel:+380973287848"><p class="h6smart">(097)328-78-48</p></a>
        </li>
        <li>
            <a href="tel:+380637613915"><p class="h6smart">(063)761-39-15</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503253519"><p class="h6smart">(050)325-35-19 +Viber</p></a></span>
        </li>
        <li>
            <p class="h6smart">u0005@automafia.com.ua</p>
        </li>
    </ul>
</li>

<li class="mainmenu"><p class="h5smart">Менеджер 4</p>
    <ul>
        <li>
            <a href="tel:+380675790829"><p class="h6smart">(067)579-08-29</p></a>
        </li>
        <li>
            <a href="tel:+380637613914"><p class="h6smart">(063)761-39-14</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503252930"><p class="h6smart">(050)325-29-30 +Viber</p></a></span>
        </li>
        <li>
            <p class="h6smart">u0004@automafia.com.ua</p>
        </li>
    </ul>
</li>

<li class="mainmenu"><p class="h5smart">Константин Шетилов</p>
    <ul >
        <li>
            <a href="tel:+380675793117"><p class="h6smart">(067)579-31-17</p></a>
        </li>
        <li>
            <a href="tel:+380637613913"><p class="h6smart">(063)761-39-13</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503463936"><p class="h6smart">(050)346-39-36 +Viber</p></a>
        </li>
        <li>
            <p class="h6smart"> u0003@automafia.com.ua</p>
        </li>
    </ul>
</li>

<li class="mainmenu"><p class="h5smart">Вячеслав Пашкевич</p>
    <ul>
        <li>
            <a href="tel:+380675793128"><p class="h6smart">(067)579-31-28</p></a>
        </li>
        <li>
            <a href="tel:+380637613911"><p class="h6smart">(063)761-39-11</p></a>
        </li>
        <li>
            <span class="violetText" ><a href="tel:+380503252937"><p class="h6smart">(050)325-29-37 +Viber</p></a>
        </li>
        <li>
            <p class="h6smart">u0001@automafia.com.ua</p>
        </li>
    </ul>
</li>
</div>





