<?php
$page_name = "Партнеры";
include("./config.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>automafia.com.ua</title>
</head>
<body>
<div id = "all" class = "all">
    <div id = "left" class = "left border">
       <?php
       include(PATH_INFO."/block/left_block.php");
       ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include (PATH_INFO."/block/header_head.php");
        ?>
        <div  class = "content border">
            <a href="<?php echo(ADRES); ?>/kyb.php"><button type="button" class="btn-primary lagbtn">KYB</button></a>
            <a href="<?php echo(ADRES); ?>/abesta.php"><button type="button" class="btn-primary lagbtn">ABESTA</button></a>
        </div>

    </div>
    <div  class = "footer  border">
        <?php
        include(PATH_INFO."/block/footer.php")
        ?>
    </div>

</div>

