<?php
$page_name = "Ответственность и правила";
include("./config.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>automafia.com.ua</title>
    <link href="<?php echo(ADRES); ?>/css/official.css" rel="stylesheet">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

</head>
<body>
<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include(PATH_INFO."/block/left_block.php");
        ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include(PATH_INFO."/block/header_head.php");
        ?>
        <div  class = "content border">
             <p class="redText h4smart">Внимание!</p>
            <p class="violetText h6smart">Во избежание недоразумений делайте заказы только  по официальным номерам компании АВТОМАФИЯ.
            Все контакты и имена наших сотрудников указаны только на этом сайте в разделе <a class = "blueText hrefLine" href="contact.php">КОНТАКТЫ</a> Остерегайтесь мошенников представляющихся АВТОМАФИЕЙ.
            </p>
            <p class="redText h4smart">
            Внимание!
                </p>
            <p class="violetText h6smart">
            При оплате нам проверяйте платёжные реквизиты по нашему сайту. Актуальные и действительные платёжные реквизиты (карточка, счёт)
            указаны только на этом сайте на страничке <a class = "blueText hrefLine" href="oplata.php"> ОПЛАТА</a> При оплате заказов на иные реквизиты компания АВТОМАФИЯ снимает с себя всякую ответственность.
            </p>
            <p class="redText h4smart">
            Внимание!
                </p>
            <p class="violetText h5smart">
            Условия заказа запчастей из-за рубежа.
            </p>
            <p>
            <div  >
                <ul>
            <li class = "decimal h6smart">Возврат запчастей заказанных за рубежом невозможен.</li>
            <li class = "decimal h6smart">Предоплата 100%. При этом окончательная цена на данный заказ фиксируется.</li>
            <li class = "decimal h6smart">В связи со сложностью доставки запчастей из-за границы (несколько транзитных складов, граница, таможня,
                человеческий фактор, удалённость склада закупки и т.д.) срок исполнения заказа является ориентировочным и
                точного срока поставки мы гарантировать не можем.</li>
            <li class = "decimal h6smart">Отказ клиента от поставки товара после оформления заказа невозможен.</li>
            <li class = "decimal h6smart">При большой задержке заказа крайним сроком ожидания является срок 6недель с момента оформления заказа.
                Только по истечении данного срока возможен возврат предоплаты.</li>
            <li class = "decimal h6smart">После заказа товара возможен отказ поставщика от поставки по различным причинам (устаревший прайс, отсутствие товара на складе, утеря товара и т.д ).</li>
            <li class = "decimal h6smart">В случае отказа поставщика от поставки возможны варианты: заказ товара с другого склада (возможно изменение цены, согласовывается с клиентом) или возврат предоплаты.</li>
            <li class = "decimal h6smart">В случае невозможности поставки предоплата возвращается в исходном размере и в той же валюте что и была внесена.</li>
            </ul>
                </div>

        </div>

    </div>
    <div  class = "footer  border">
        <?php
        include(PATH_INFO."/block/footer.php")
        ?>
    </div>

</div>

