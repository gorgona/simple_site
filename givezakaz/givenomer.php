<?php
$page_name = "Как получить заказ по номеру";
include("../config.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>automafia.com.ua</title>
    <link href="../css/official.css" rel="stylesheet">
    <script type="text/javascript" src="../js/adaptive.js"></script>
</head>
<body>
<div id = "all" class = "all">
    <div id = "left" class = "left border">
        <?php
        include(PATH_INFO."/block/left_block.php");
        ?>
    </div>

    <div id = "right" class = "right border">
        <?php
        include(PATH_INFO."/block/header.php");
        include(PATH_INFO."/block/header_head.php");
        ?>
        <div  class = "content border">
            <h1 class = "redText" >УВАЖАЕМЫЕ ПОКУПАТЕЛИ! ПРИ ПОЛУЧЕНИИ ТОВАРА ОБЯЗАТЕЛЬНО СВЕРЯЙТЕ НОМЕРА ЗАПЧАСТЕЙ И ИХ КОЛИЧЕСТВО С ВАШЕЙ РАСХОДНОЙ НАКЛАДНОЙ </h1>

            <h1 class = "redText" > Продиктуйте номер вашего заказа по телефону ниже указанным сотрудникам :</h1>

            <?php
            include(PATH_INFO."/block/contactKassa.php");
            ?>
            <a href="<?php echo(ADRES); ?>give-zakaz.php"><button type="button" class="btn btn-primary btn-lg lagbtn"><h1>НАЗАД</h1></button></a>
          </div>

</div>
</div>
</body>