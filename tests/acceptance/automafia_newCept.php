<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('See all pages');


$I->amOnPage('/index.php');
$I->see('Главная' );
$I->see('u0001@automafia.com.ua' );
$I->see('СТО:' );
$I->see('Принимаем запчасти на реализацию' );
$I->seeElement('//img[@src="./images/image008.jpg"]');

$I->amOnPage('/assorti.php');
$I->see('Ассортимент' );
$I->see('Принимаем запчасти на реализацию' );

$I->amOnPage('/svidoctvo.php');
$I->see('Документы' );

$I->amOnPage('/svidoctvo.php');
$I->see('Документы' );
$I->seeElement('//img[@src="./images/svidoctvo/02/vipiska1.jpg"]');
$I->seeElement('//img[@src="./images/svidoctvo/02/vipiska2.jpg"]');
$I->seeElement('//img[@src="./images/svidoctvo/02/vityag-z-EDR1.jpg"]');
$I->seeElement('//img[@src="./images/svidoctvo/02/vityag-z-EDR2.jpg"]');
$I->seeElement('//img[@src="./images/svidoctvo/02/vityag-z-EDR3.jpg"]');
$I->seeElement('//img[@src="./images/svidoctvo/02/vityag-z-reestry.jpg"]');


$I->amOnPage('/transport.php');
$I->see('Доставка в регионы' );
$I->see('Осуществляем доставку по Харькову' );
$I->see('Принимаем запчасти на реализацию' );

$I->amOnPage('/contact.php');
$I->see('Контакты' );
$I->see('u0001@automafia.com.ua' );
$I->see('СТО:' );
$I->see('Принимаем запчасти на реализацию' );

$I->amOnPage('/partner.php');
$I->see('Партнеры' );
$I->see('KYB' );
$I->see('ABESTA' );

$I->amOnPage('/oplata.php');
$I->see('Оплата' );
$I->see('Безналичный расчёт (UAH)' );
$I->see('Владелец счёта:' );
$I->see('ОТВЕТСТВЕННОСТЬ' );
$I->see('Принимаем запчасти на реализацию' );

$I->amOnPage('/pravila.php');
$I->see('Ответственность и правила' );
$I->see('Остерегайтесь мошенников представляющихся АВТОМАФИЕЙ' );
$I->see('Условия заказа запчастей из-за рубежа' );

$I->amOnPage('/poddelki.php');
$I->see('Подделки' );

$I->amOnPage('/job.php');
$I->see('Вакансии' );
$I->see('1. Менеджер по продажам автозапчастей' );
$I->see('Принимаем запчасти на реализацию' );
